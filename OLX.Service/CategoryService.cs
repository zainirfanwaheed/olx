﻿using System;
using OLX.Entity;
using OLX.Databse;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OLX.Service
{
    public class CategoryService
    {
        public Category GetCategory(int ID)
        {
            using (var context = new OLX_DB())
            {
                return context.Categories.Find(ID);
            }
        }

        public List<Category> GetCategories()
        {
            using (var context = new OLX_DB())
            {
                return context.Categories.ToList();
            }
        }
        public void SaveCategory(Category category)
        {
            using (var context = new OLX_DB())
            {
                context.Categories.Add(category);
                context.SaveChanges();
            }
        }
        public void UpdaeCategory(Category category)
        {
            using (var context = new OLX_DB())
            {
                context.Entry(category).State=System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
        public void DeleteCategory(int ID)
        {
            using (var context = new OLX_DB())
            {
                var category = context.Categories.Find(ID);
                context.Entry(category).State = System.Data.Entity.EntityState.Deleted;
                //Same as above
                //context.Categories.Remove(category);
                context.SaveChanges();
            }
        }
    }
}
