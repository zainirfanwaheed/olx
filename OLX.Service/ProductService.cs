﻿using System;
using OLX.Entity;
using OLX.Databse;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OLX.Service
{
    public class ProductService
    {
        public Product GetProduct(int ID)
        {
            using (var context = new OLX_DB())
            {
                return context.Products.Find(ID);
            }
        }

        public List<Product> GetProducts()
        {
            using (var context = new OLX_DB())
            {
                return context.Products.ToList();
            }
        }
        public void SaveProduct(Product product)
        {
            using (var context = new OLX_DB())
            {
                context.Products.Add(product);
                context.SaveChanges();
            }
        }
        public void UpdaeProduct(Product product)
        {
            using (var context = new OLX_DB())
            {
                context.Entry(product).State=System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
        public void DeleteProduct(int ID)
        {
            using (var context = new OLX_DB())
            {
                var product = context.Products.Find(ID);
                context.Entry(product).State = System.Data.Entity.EntityState.Deleted;
                //Same as above
                //context.Categories.Remove(category);
                context.SaveChanges();
            }
        }
        public List<Product> SearchProduct(string search)
        {
            using (var context = new OLX_DB())
            {
                return context.Products.Where(p=> p.Name != null && p.Name.ToLower().Contains(search.ToLower())).ToList();
            }
        }
    }
}
