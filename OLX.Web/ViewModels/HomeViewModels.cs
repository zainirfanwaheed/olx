﻿using OLX.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OLX.Web.ViewModels
{
    public class HomeViewModels
    {
        public List<Category> Categories { get; set; }
    }
}