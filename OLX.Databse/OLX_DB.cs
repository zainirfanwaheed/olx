﻿using OLX.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OLX.Databse
{
     public class OLX_DB : DbContext, IDisposable
    {
        public OLX_DB() : base("OLX_Project")
        { }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}
